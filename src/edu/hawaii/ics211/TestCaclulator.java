/**
 * Test for Calculator.
 * @author ZacharyMartin
 *
 */
public class TestCaclulator {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		System.out.println(calculator.add(1, 2));
		System.out.println(calculator.subtract(10, 5));
		System.out.println(calculator.divide(25, 5));
		System.out.println(calculator.modulo(100, 3));
		System.out.println(calculator.multiply(14, 6));
		System.out.println(calculator.pow(2, 4));
	}
}
